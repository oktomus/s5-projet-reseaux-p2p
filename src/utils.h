/*
 * utils.h
 *
 *  Created on: Nov 1, 2016
 */

#ifndef SRC_UTILS_H_
#define SRC_UTILS_H_



/**
* Prints a message and stop the program
*
* @arg message 	char*		message to print
* @arg err_code int		error code to exit
*/
void fail(const char* message, int err_code);

/**
* Prints a message with perror and stop the program
*
* @arg message 	char*		message to print
*/
void failp(const char* message);


#endif /* SRC_UTILS_H_ */
