#include "utils.h"
#include "trackerlib.h"
#include "peerslib.h"


#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <time.h>
#include <signal.h>


void recycle_tracker_clients(struct tracker_clients * buf){
	struct tracker_unique_client * ptr = buf->first;
	struct tracker_unique_client * previous = NULL;
	int now = (int) time(0);
	while(ptr != NULL){
		if(now - ptr->id >= TRACKER_BUFFER_TIMEOUT){
			printf("No keep-alive, delete %s on %s port %d\n", ptr->hash.hash, get_addr_string(ptr->client.address), ptr->client.port); 
			if(previous == NULL) buf->first = ptr->next;
			else previous->next = ptr->next;

			if(buf->last == ptr) buf->last = previous;

			// Delete client from buffer
			struct tracker_unique_client * next = ptr->next;
			free(ptr);
						
			
			ptr = next;			
			--buf->count;
		}else{

			previous = ptr;
			ptr = ptr->next;
		}
	}

}

void keep_alive_tracker_client(struct tracker_clients * buf, struct client cl, struct file_hash hs){
	
	struct tracker_unique_client * ptr = buf->first;
	while(ptr != NULL){
		if(strcmp(ptr->client.address, cl.address) == 0 && strcmp(ptr->hash.hash, hs.hash) == 0 && ptr->client.port == cl.port){
			ptr->id = (int) time(0);
			break;
		}

		ptr = ptr->next;
	}

}

void ajouter_client(struct tracker_clients * buf, struct client cl, struct file_hash hs){
	struct tracker_unique_client * ajout;
	ajout = (struct tracker_unique_client *) malloc(
			sizeof(struct tracker_unique_client)
			+ sizeof(unsigned char) * (cl.length - 2)
			+ sizeof(char) * (hs.size));
	memcpy(&(ajout->client), &cl, sizeof(struct client) + sizeof(unsigned char) * (cl.length - 2));
	memcpy(&(ajout->hash), &hs, sizeof(struct file_hash) + sizeof(char) * hs.size);
	memcpy(&(ajout->client.address), &(cl.address), cl.length - 2);
	memcpy(&(ajout->hash.hash), &(hs.hash), hs.size);
	ajout->id = (int) time(0);
	ajout->next = NULL;	
	if(DEBUG) printf("Ajout du client %d %s au buffer\n", ajout->id, ajout->hash.hash);
	if(buf->first == NULL){
		buf->first = ajout;
		buf->last = ajout;
	}else{
		buf->last->next = ajout;
		buf->last = ajout;
	}
	buf->count += 1;
}

void afficher_tracker_clients(struct tracker_clients * buf){
	struct tracker_unique_client * ptr = buf->first;
	while(ptr != NULL){
		printf("%s on %s port %d\n", ptr->hash.hash, get_addr_string(ptr->client.address), ptr->client.port);
		ptr = ptr->next;
	}
	printf("-------\n");
}

int enlever_client(struct tracker_clients * buf, int id){
	struct tracker_unique_client * ptr = buf->first;
	struct tracker_unique_client * last = NULL;
	while(ptr != NULL){
		if(ptr->id == id){
			if(buf->first == ptr){
				buf->first = NULL;
				buf->last = NULL;
			}else if(buf->last == ptr){
				buf->last = last;
			}
			if(DEBUG) printf("Suppression du client %d du buffer\n", ptr->id);
			last->next = ptr->next;
			free(ptr);
			buf->count -= 1;
			return 1;
		}
		last = ptr;
		ptr = ptr->next;
	}
	if(DEBUG) printf("Suppression : Client %d non dans le buffer\n", id);
	return 0;

}


