#include <stdio.h>
#include <stdlib.h>

/**
 * Programme test pour la fragmentation de fichier
 * frag_file FILE CHUNK_INDEX
 *
 * Pour REP_GET
 */

int main(int argc, const char * argv[]){
	
	int required_chunk = atoi(argv[2]);
	FILE * file = fopen(argv[1], "r");

	fseek(file, 0, SEEK_END);
	int size = ftell(file);
	fseek(file, 0, SEEK_SET);

	int nbChunk = size / 1000000 + 1;

	int maxIndex;
	if(required_chunk == nbChunk){
		maxIndex = (size - (required_chunk - 1) * 1000000 ) / 1000 + 1;
	}else{
		maxIndex = 1000000/1000 + 1;
	}

	printf("Nb chunk : %d\nRequired chunk : %d\nMax index : %d\nFile size : %d\n", nbChunk, required_chunk, maxIndex, size);

	fseek(file, (required_chunk -1) * 1000000, SEEK_SET);

	unsigned char buffer[1000];
	int nbItemsRead = 0;
	int index = 1;
	while((nbItemsRead = fread(buffer, 1, 1000, file))){
		buffer[10] = '\0';
		printf("Index %d, %d bytes : %s...\n", index, nbItemsRead, buffer);
		++index;
	}


	fclose(file);


	return 0;
}

