#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

void fail(const char* message, int err_code){

    printf("%s\n", message);
    exit(err_code);

}

void failp(const char* message){
	perror(message);
	exit(EXIT_FAILURE);
}
