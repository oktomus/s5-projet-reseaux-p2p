/*
 * hash-lib.h
 *
 *  Created on: Nov 1, 2016
 */

#ifndef SRC_HASHLIB_H_
#define SRC_HASHLIB_H_

#include <openssl/sha.h>

#define SHA256_HASH_LENGTH (SHA256_DIGEST_LENGTH * 2 + 1) // String size require to store a sha256 hash

#define CHUNK_SIZE 1000000 // The file chunks size in bytes


/**
 * Returns the number of chunk of a file
 *
 * If the file size is lower than the chunk size, then the result is 1
 * Else, the result is the size divided by the chunk size
 *
 * @arg input 	const char*			The file path
 */
int fchunks(const char * file_url);

/**
 * Create the sha 256 hash of a file
 *
 * @arg input 	const char*			The file path
 * @arg output  char*				Where to output the hash as a string
 */
void file_sha256(const char * file_url, char output[SHA256_HASH_LENGTH]);

/**
 * Create the sha 256 hash of a file chunk
 *
 * @arg input 	const char*			The file path
 * @arg output  char*				Where to output the file's chunk hash as a string
 * @arg chunk_index const int		The chunk to hash, the first chunk is 1
 */
void file_chunk_sha256(const char * file_url, char output[SHA256_HASH_LENGTH], const int chunk_index);


/**
 * Create the sha 256 print of a given string
 *
 * @arg input 	const char*			The model string used to create the hash
 * @arg output  char*				Where to output the hash as a string
 */
void string_sha256(const char * input, char output[SHA256_HASH_LENGTH]);


/**
 * Transfer a sha256 hash into a string
 *
 * @arg hash 	unsigned char*		The hash, an array of bytes
 * @arg output  char*				Where to output the string
 */
void sha256_to_string(unsigned char hash[SHA256_DIGEST_LENGTH], char output[SHA256_HASH_LENGTH]);

void string_sha256_l(const char * input, char output[SHA256_HASH_LENGTH], const int size);

#endif /* SRC_HASHLIB_H_ */
