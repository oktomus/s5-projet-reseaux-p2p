/*
 * peerslib.h
 *
 *  Created on: Nov 5, 2016
 */

#ifndef SRC_PEERSLIB_H_
#define SRC_PEERSLIB_H_

#include "utils.h"
#include "hashlib.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <unistd.h>


#define DEBUG 				0
#define BUFFER_SIZE 			2024


// Message types

// Communication entre pairs

#define T_PP_GET 			100 // hash f ichier + hash chunk / Demander un chunk d’un fichier
#define T_PP_REP_GET 			101 // hash f ichier + hash chunk + chunk / Récupérer un chunk d’un fichier (voir 5.1)
#define T_PP_REP_GET_ACK		105 // Ack d'un fragment
#define T_PP_LIST			102 // hash f ichier / Récupérer la liste des chunks d’un fichier
#define T_PP_REP_LIST			103 // hash f ichier + liste chunks / Liste des chunks d’un fichier

// Communication client tracker

#define T_CT_PUT			110 // hash f ichier + client / Indiquer qu’on possède un fichier
#define T_CT_ACK_PUT			111 // hash f ichier + client / Confirmer l’ajout du client
#define T_CT_GET			112 // hash f ichier + client / Demander la liste des clients qui possèdent ce fichier
#define T_CT_ACK_GET			113 // hash f ichier + liste clients / Liste des clients qui possèdent ce fichier
#define T_CT_KEEP_ALIVE			114 // hash f ichier / Annoncer au tracker qu’on gère toujours ce fichier
#define T_CT_ACK_KEEP_ALIVE		115 // hash f ichier / Acquittement du message KEEP_ALIVE

// Debug tracker

#define T_D_DEBUG			150 // / Liste les fichiers et les clients associés dans la console

// Hash

#define T_H_FILE			50 // size + hash
#define T_H_CHUNK			51 // size + hash

// Client

#define T_C_CLIENT			55 // 6 or 18 + port + addr ip

// Fichier

#define T_F_FRAGMENT			60 // 4 to 1004 + index + max index + data

struct fragment{
	unsigned char type;
	unsigned short size;
	unsigned short index;
	unsigned short max_index;
	char * data;
};

struct file_hash{
	unsigned char type;
	unsigned short size;
	char * hash;
}; // 3 bytes + size(hash) bytes

struct chunk_hash{
	unsigned char type;
	unsigned short size;
	unsigned short index; // Apres sérialisation, se retrouve a la fin du tampon
	char * hash;
}; // 5 bytes + size(hash) bytes - 2

struct chunk_hash_chained{
	struct chunk_hash * value;
	struct chunk_hash_chained * next;
};

struct client{
	unsigned char type;
	unsigned short length; // 6 or 18 /!\ Un char peut etre utilise
	unsigned short port;
	unsigned char * address;
	//En utilisant un tableau de unsigned char,
	// on est sûr de pouvoir insérer une adresse IPv4 ou IPv6.
	//Pour IPv4 : 192.168.88.62 ==> ['192', '168', '88', '62']
	//Pour IPv6 : 2001:0db8:0000:85a3:0000:0000:ac1f:8001
	//==> ['20', '01', '0d', 'b8' , '00', '00', '85', 'a3', '00' , '00', '00', '00', 'ac', '1f', '80', '01']
}; // 5 bytes + (16 or 4 bytes)

struct client_chained{
	struct client * value;
	struct client_chained * next;
};

struct c_file_hash_liste_client{
	struct file_hash * fh;
	struct client_chained * first;
};

struct c_file_hash_liste_chunk{
	struct file_hash * fh;
	int size; // Ne sera pas sérialisé
	struct chunk_hash_chained * first;
};

struct c_file_chunk_hash{
	struct file_hash * fh;
	struct chunk_hash * ch;
};

struct c_file_hash_client{
	struct file_hash fh;
	struct client c;
}; // size(file_hash) + size(client) bytes

struct c_fragment{
	struct file_hash *fh;
	struct chunk_hash *ch;
	struct fragment *frg;
};

struct message{
	unsigned char type;
	unsigned short length;
	void * data;
}; // 3 bytes + size(data) bytes

struct paquet{
	struct message msg;
}; // size(message) bytes

unsigned char * serialize_unshort(unsigned char *buffer, unsigned short s);

unsigned char * serialize_string(unsigned char *buffer, unsigned char *c, unsigned short taille);


unsigned char * serialize_unchar(unsigned char *buffer, unsigned char c);

unsigned char * serialize_file_hash(unsigned char *buffer, struct file_hash * fh);


unsigned char * serialize_client(unsigned char *buffer, struct client * value);

unsigned char * serialize_message(unsigned char *buffer, struct message *msg);

unsigned char * serialize_paquet(unsigned char *buffer, struct paquet *value);

unsigned short unserialize_unshort(unsigned char **buffer);

unsigned char unserialize_unchar(unsigned char **buffer);

unsigned char * unserialize_string(unsigned char **buffer, unsigned short taille);

struct file_hash unserialize_file_hash(unsigned char **buffer);

struct client unserialize_client(unsigned char **buffer);

struct message unserialize_message(unsigned char **buffer);

struct paquet unserialize_paquet(const char *buffer);


struct sockaddr_in6;
struct in6_addr;

/**
 * Create a sockadd_in6 with given attributes
 *
 * @arg sock		A pointer to the sock to initialize
 * @arg port		The port of the socket
 * @arg sock_family The address family
 * @arg address		The address, can be null
 * @return 			0 if the initialisation was ok, else -1
 */
int create_sock6(struct sockaddr_in6 *sock, const char* port, sa_family_t sock_family, const char* address);

/**
 * Get the string value of a socket address
 *
 * @arg sock		A pointer to the socket address
 */
char * get_addr_string(const void *src);

void old_get_addr_string6(struct sockaddr_in6 *sock, char * output);


struct paquet take_paquet(int sockfd, struct sockaddr * src, socklen_t * addrlen);

void send_paquet(struct paquet, int sockfd, struct sockaddr * src, socklen_t addrlen);


//////////////////////
// Methodes de debug

void debug_paquet_serial(struct paquet p);

void debug_paquet_unserial(char * buff, ssize_t taille);

#endif /* SRC_PEERSLIB_H_ */
