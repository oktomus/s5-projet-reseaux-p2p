/*
 * q1-2_client.c
 *
 *  Created on: Nov 4, 2016
 */

#include "utils.h"
#include "peerslib.h"
#include "trackerlib.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <signal.h>



int main( int argc, const char* argv[] )
{

	// Check for args
	if( argc != 6 ){
		fail("USAGE: $q1-2_client tracker_address tracker_port listen_port put/get file-hash", EXIT_FAILURE);
	}

	const char * hash = argv[5];

	// Creation des socket ecoute/envoie/reception
	int sockfd = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP); // Socket
	if(sockfd == -1)
	{
		failp("A problem has occurred while creating the socket");
	}

	struct sockaddr_in6 me;
	if(create_sock6(&me, argv[3], AF_INET6, NULL) < 0){
		failp("Can't create the socket addr for the client");
	}

	struct sockaddr_in6 tracker;
	if(create_sock6(&tracker, argv[2], AF_INET6, argv[1]) < 0){
		failp("Can't create the socket addr for the tracker");
	}

	struct sockaddr_in6 client; // Sock addresse message recu
	memset (&client, 0, sizeof (struct sockaddr_in6));

	socklen_t addrlen = sizeof(struct sockaddr_in6); // Taille adresse


	// Output terminal
	char * tracker_addr = get_addr_string(&tracker.sin6_addr);
	char * my_addr = get_addr_string(&me.sin6_addr);
	if(tracker_addr == NULL){
		failp("Cannot get the address value");
	}
	printf("listening on %s\n", argv[3]);

	// bind addr structure with socket
	if(bind(sockfd, (struct sockaddr *) &me, addrlen) == -1)
	{
		perror("bind");
		close(sockfd);
		exit(EXIT_FAILURE);
	}


	// Fonction de fermeture signal close
	void close_socket(int signo){
		if(signo == SIGINT){
			close(sockfd);
			exit(0);
		}
	}

	if(signal(SIGINT, close_socket) == SIG_ERR)
		fail("Can't bind SIGINT", EXIT_FAILURE);


	// Envoie

	struct file_hash fh = {
		(unsigned char)T_H_FILE,
		strlen(hash),
		(char *)malloc(sizeof(char) * strlen(hash))
	};
	strcpy(fh.hash, hash);

	// TO DO : addresse client, trouver par quelle adresse on va contacter le tracker

	struct client c = {
		(unsigned char)T_C_CLIENT,
		18,
		atoi(argv[3]),
		me.sin6_addr.s6_addr
	};

	struct c_file_hash_client data = {
		fh, c
	};

	struct message msg = {
		0,
		0,
		(void *) &data
	};

	if(strcmp(argv[4], "put") == 0){
		msg.type = (unsigned char)T_CT_PUT;
		printf("put %s to %s port %d\n", fh.hash, tracker_addr, atoi(argv[2]));
	}else if(strcmp(argv[4], "get") == 0){
		msg.type = (unsigned char)T_CT_GET;
		printf("get %s from %s port %d\n", fh.hash, tracker_addr, atoi(argv[2]));
	}else{
		printf("Action must be put or get.\n");
		exit(EXIT_FAILURE);
	}

	struct paquet p = {
		msg
	};

	//if(DEBUG) debug_paquet_serial(p);


	// Envoie de la demande (put/get)
	while(1){
		send_paquet(p, sockfd, (struct sockaddr *) &tracker, addrlen);

		struct paquet p_recu = take_paquet(sockfd, (struct sockaddr *) &client, &addrlen);

		if(DEBUG) debug_paquet_serial(p_recu);

		if(p_recu.msg.type == p.msg.type +1){ // On envoie plus si on a le ACK

			if(p_recu.msg.type == T_CT_ACK_GET){
				struct c_file_hash_liste_client * data = (struct c_file_hash_liste_client * ) p_recu.msg.data;
				struct client_chained * ptr = data->first;
				while(ptr != NULL){
					struct client * c = ptr->value;
					printf("IP : %s port %d\n", get_addr_string(c->address), c->port);
					ptr = ptr->next;
				}
				break; // Fin
			}else if(p_recu.msg.type == T_CT_ACK_PUT || p_recu.msg.type == T_CT_ACK_KEEP_ALIVE){ // On renvoi des keep alive
				p.msg.type = T_CT_KEEP_ALIVE;	
				sleep(TRACKER_BUFFER_TIMEOUT - 3);

			}

		}

	}

	// close the socket
	close(sockfd);

	exit(0);

}
