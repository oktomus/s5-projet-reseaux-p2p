#include "utils.h"
#include "peerslib.h"
#include "trackerlib.h"
#include "hashlib.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <signal.h>
#include <errno.h>


int main( int argc, const char* argv[] )
{

	// Check for args
	if( argc != 4 ){
		fail("USAGE: $q1-4_seeder listend_address listen_port file", EXIT_FAILURE);
	}

	

	// Creation des socket ecoute/envoie/reception
	int sockfd = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP); // Socket
	if(sockfd == -1) failp("A problem has occurred while creating the socket");

	struct sockaddr_in6 me;
	if(create_sock6(&me, argv[2], AF_INET6, argv[1]) < 0) failp("Can't create the socket addr for the seeder");

	struct sockaddr_in6 listen; // Sock addresse message recu

	socklen_t addrlen = sizeof(struct sockaddr_in6); // Taille adresse

	// Get the hash
	char hash[SHA256_HASH_LENGTH];
	int nbChunk = fchunks(argv[3]);
	char ** chunks_hash = (char **) malloc(sizeof(char *) * nbChunk);

	file_sha256(argv[3], hash);	
	// Output terminal
	printf("listening on %s port %d, file %s, hash %s\n", get_addr_string(&me.sin6_addr), atoi(argv[2]), argv[3], hash);
	printf("[ ");
	int i;
	for(i = 0; i < nbChunk; ++i){
		chunks_hash[i] = (char *) malloc(sizeof(char) * SHA256_HASH_LENGTH);
		file_chunk_sha256(argv[3], chunks_hash[i], i+1);
		printf("%s ", chunks_hash[i]);
	}
	printf("]\n");

	// bind addr structure with socket
	if(bind(sockfd, (struct sockaddr *) &me, addrlen) == -1)
	{
		perror("bind");
		close(sockfd);
		exit(EXIT_FAILURE);
	}


	// Fonction de fermeture signal close
	void close_socket(int signo){
		if(signo == SIGINT){
			close(sockfd);
			exit(0);
		}
	}

	if(signal(SIGINT, close_socket) == SIG_ERR)
		fail("Can't bind SIGINT", EXIT_FAILURE);


	while(1){
		struct paquet p_recu = take_paquet(sockfd, (struct sockaddr *) &listen, &addrlen);


		if(p_recu.msg.type == T_PP_LIST){ // Demande de liste des chunks
			if(DEBUG) printf("Recevied a LIST request\n");
			struct file_hash * data = (struct file_hash *) p_recu.msg.data;

			if(DEBUG) debug_paquet_serial(p_recu);
			struct c_file_hash_liste_chunk * new_data = (struct c_file_hash_liste_chunk *) malloc(
					sizeof(struct c_file_hash_liste_chunk));

			new_data->size = 0;
			new_data->first = NULL;
			new_data->fh = data;

			if(memcmp(data->hash, hash, data->size) == 0){ // Si on a le chunk demandé
				struct chunk_hash_chained * last = NULL;
				for(i = 0; i < nbChunk; ++i){
					int hash_size = strlen(chunks_hash[i]);
					struct chunk_hash * v = (struct chunk_hash *) malloc(sizeof(struct chunk_hash));
				       	v->hash = (char *) malloc(hash_size * sizeof(char));
					struct chunk_hash_chained * ptr = (struct chunk_hash_chained *) malloc(sizeof(struct chunk_hash_chained));
					ptr->value = v;
					ptr->next = NULL;
					v->index = i+1;
					v->type = T_H_CHUNK;
					v->size = hash_size + 2;
					//printf("chunk index %d size %d type %d value %s\n", v->index, v->size, v->type, chunks_hash[i]);
					memcpy(v->hash, chunks_hash[i], hash_size);

					if(new_data->first == NULL){
						new_data->first = ptr;
					}

					if(last != NULL) last->next = ptr;

					last = ptr;

					++(new_data->size);
				}
			}

			p_recu.msg.data = (void *) new_data;
			p_recu.msg.type = T_PP_REP_LIST;
			//if(DEBUG) debug_paquet_serial(p_recu);
			send_paquet(p_recu, sockfd, (struct sockaddr *) &listen, addrlen); 	// Envoie du paquet
		}else if(p_recu.msg.type == T_PP_GET){ // Demande de chunk
			if(DEBUG) printf("Reveived a CHUNK request\n");
			struct c_file_chunk_hash * data = (struct c_file_chunk_hash *) p_recu.msg.data;


			if(memcmp(data->fh->hash, hash, data->fh->size) == 0) { // Si on a le fichier demandé
				
				struct fragment frg = {
					T_F_FRAGMENT,
					0,
					0,
					0,
					NULL
				};

				struct c_fragment send_data= {
					data->fh,
					data->ch,
					&frg					
				};

				struct paquet p_envoi = {
					{T_PP_REP_GET, 0, (void *) &send_data}
				};

				FILE *file;
				file = fopen(argv[3], "r");
				if (!file) perror("Can't open the file"); // Si fopen n'a pas marcher


				// Calcul du nombre de fragement
				if(data->ch->index == nbChunk){
					fseek(file, 0, SEEK_END);
					long size = ftell(file);
					fseek(file, 0, SEEK_SET);
					int seg = (size - (data->ch->index - 1) * CHUNK_SIZE);
					frg.max_index = seg / 1000;
					if(frg.max_index * 1000 < seg)
						++frg.max_index;
				}else{
					frg.max_index = CHUNK_SIZE / 1000;
					
				}
				fseek(file, (data->ch->index -1) * 1000000, SEEK_SET); // On offset les premiers chunks
				
				printf("Envoie du chunk %d, offset : %ld, max_index : %d : %s\n", data->ch->index, ftell(file), frg.max_index, data->ch->hash);

				unsigned char buffer[BUFFER_SIZE];
				memset(buffer, '\0', BUFFER_SIZE);
				int nbItemsRead = 0;
				int index = 1;
				//char chunk_fname[30];
				//sprintf(chunk_fname, "seed_%d.chunk", data->ch->index);
				//FILE * chunk_write = fopen(chunk_fname, "w");

				while(index <= frg.max_index && (nbItemsRead = fread(buffer, 1, 1000, file))){ // Pour chaque fragment de 1000 bytes
					//printf("Min index : %d, index : %d, max index : %d\n", 1, index, frg.max_index);
					//printf("ENVOIE DE \n\n\nDEBUT%sFIN\n\n\n", buffer);
					//printf("ENVOIE DE \n\n\nDEBUT--");
					/*int k;
					for(k =0; k<nbItemsRead; ++k) {
						printf("%c", buffer[k]);
					}
					printf("--FIN\n\n\n");*/
					frg.index = index;
					frg.size = nbItemsRead + 4;
					frg.data = buffer;
					//if(DEBUG) debug_paquet_serial(p_envoi);
					int transmit = 0;
					/*for(i=0; i < nbItemsRead; ++i){
						putc(buffer[i], chunk_write);
					}*/
					while(!transmit){ // Attente du ACK
						send_paquet(p_envoi, sockfd, (struct sockaddr *) &listen, addrlen); 	// Envoie du paquet
							
						struct paquet ack = take_paquet(sockfd, (struct sockaddr *) &listen, &addrlen);

						if(ack.msg.type = T_PP_REP_GET_ACK){
							struct c_fragment * ack_data = (struct c_fragment *) ack.msg.data;
							if(ack_data->frg->index == index) transmit = 1;
						}
					}
					++index;
					memset(buffer, '\0', BUFFER_SIZE);

				}


				
				//fclose(chunk_write);

				fclose(file);

				//exit(0);

			}

			
		}




	}

	// close the socket
	close(sockfd);

	exit(0);

}
