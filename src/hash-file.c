/**
* @title: hash-file.c
* 
* @description:
*       Program used to create a file and its chunks print
*       The algorithm used to generate the prints is SHA 256
*/

#include "hashlib.h"
#include "utils.h"
#include <stdio.h>


int main( int argc, const char* argv[] )
{
	
    // Check for args
    if( argc != 2 ){
        fail("USAGE: $hash-file file", 101);
    }

    char file_hash[SHA256_HASH_LENGTH];
    int nbChunk = fchunks(argv[1]);
    char chunks_hash[nbChunk][SHA256_HASH_LENGTH];

    file_sha256(argv[1], file_hash);

	printf("FILE HASH : %s\n", file_hash);

	int i=1;
	for (i = 1; i <= nbChunk; ++i) {
		file_chunk_sha256(argv[1], chunks_hash[i-1], i);
		printf("CHUNK %d : %s\n", i, chunks_hash[i-1]);
	}

	return 0;
}
