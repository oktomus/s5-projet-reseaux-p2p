#include "utils.h"
#include "peerslib.h"
#include "trackerlib.h"
#include "hashlib.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <time.h>
#include <signal.h>
#include <errno.h>

int main( int argc, const char* argv[] )
{

	// Check for args
	if( argc != 6 ){
		fail("USAGE: $q1-4_leecher seeder-address seeder-port listen-port file-hash file-name", EXIT_FAILURE);
	}

	const char * save_filename = argv[5];

	// Test si un fichier existe deja avec le nom donné
	FILE *file_exist;
	if ((file_exist = fopen(save_filename, "r")) == NULL) {
		if (errno == ENOENT) {
			// Le fichier n'existe pas : cool
		} else {
			failp("Impossible d'écrire dans le fichier de sauvegarde");
		}
	} else {
		fclose(file_exist);
		fail("Le fichier de sauvegarde choisi existe déjà", EXIT_FAILURE);
	}

	// Creation du socket

	int sockfd = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
	if(sockfd == -1) failp("A problem has occurred while creating the socket");

	// Creation des sockets ecout/envoi

	struct sockaddr_in6 me;
	if(create_sock6(&me, argv[3], AF_INET6, NULL) < 0) failp("Can't create the socket addr for the leecher");

	struct sockaddr_in6 seeder;
	if(create_sock6(&seeder, argv[2], AF_INET6, argv[1]) < 0) failp("Can't create the socket for the seeder");

	struct sockaddr_in6 listen;

	socklen_t addrlen = sizeof(struct sockaddr_in6); // Taille adresse

	// Output terminal

	printf("connection to %s port %d, getting %s", get_addr_string(&seeder.sin6_addr), atoi(argv[2]), argv[4]);
	printf("\non port %d", atoi(argv[3]));
	printf("\nrecorded filename %s\n", save_filename);


	// bind addr structure with socket
	if(bind(sockfd, (struct sockaddr *) &me, addrlen) == -1)
	{
		perror("bind");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	// Fonction de fermeture signal close
	void close_socket(int signo){
		if(signo == SIGINT){
			close(sockfd);
			exit(0);
		}
	}

	if(signal(SIGINT, close_socket) == SIG_ERR)
		fail("Can't bind SIGINT", EXIT_FAILURE);

	struct file_hash fh = {
		T_H_FILE,
		strlen(argv[4]),
		(char *)malloc(sizeof(char) * strlen(argv[4]))
	};
	strcpy(fh.hash, argv[4]);

	struct paquet p_get_list = {
		{T_PP_LIST, 0, (void *) &fh}
	};

	int nb_chunk = -1;
	//char chunks_hash[nbChunk][SHA256_HASH_LENGTH];
	char ** chunks_hash; // Malloc necessaire à la recuperation des hash
	
	//if(DEBUG) debug_paquet_serial(p_get_list);

	// Ecoute
	while(1){        // Premier cycle : recuperation de la liste des chunks
		send_paquet(p_get_list, sockfd, (struct sockaddr *) &seeder, addrlen);		// reception d'un paquet

		struct paquet p_recu = take_paquet(sockfd,  (struct sockaddr *) &listen, &addrlen);

		if(p_recu.msg.type == T_PP_REP_LIST){
			struct c_file_hash_liste_chunk * data = (struct c_file_hash_liste_chunk *) p_recu.msg.data;
			struct chunk_hash_chained * ptr = data->first;
			nb_chunk = data->size;
			chunks_hash = (char **) malloc(sizeof(char *) * nb_chunk);
			while(ptr != NULL){
				chunks_hash[ptr->value->index-1] = (char *) malloc(sizeof(char) * (ptr->value->size - 2));
				memcpy(chunks_hash[ptr->value->index-1], ptr->value->hash, ptr->value->size - 2);
				//printf("got chunk hash %s\n", chunks_hash[ptr->value->index-1]);
				ptr = ptr->next;
			}
		}

		if(nb_chunk != -1) break;
	}

	printf("get %d chunk(s) from %s port %s on %s\n", nb_chunk, get_addr_string(&seeder.sin6_addr), argv[2], argv[3]);
	// Il est temps de recup ces chunks
	
	if(nb_chunk>0){
		
		int acquis[nb_chunk];
		int i;
		for(i = 0; i < nb_chunk ; ++i) acquis[i] = 0;
		
		char *chunks_content[nb_chunk];
		int chunks_size[nb_chunk];
		
		int fini = 0;
		int chunk_process = 0;
		while(!fini){
			
			// RANDOM while(acquis[(chunk_process = rand() % nb_chunk)]); // Choix d'un chunk au hasard parmis ceux non acquis
			if(acquis[chunk_process]) while(acquis[(chunk_process = (chunk_process + 1) % nb_chunk)]);

			printf("recuperation du chunk %d (%s)\n", chunk_process+1, chunks_hash[chunk_process]);

			struct chunk_hash ch = {
				T_H_CHUNK,
				strlen(chunks_hash[chunk_process]) + 2,
				chunk_process+1,
				(char *) malloc(sizeof(char) * strlen(chunks_hash[chunk_process]))
			};
			strcpy(ch.hash, chunks_hash[chunk_process]);

			struct c_file_chunk_hash data = {&fh, &ch};

			struct paquet p_get_chunk = {
				{T_PP_GET, 0, (void *) &data}
			};

			char ** fragments = NULL;
			int * fragments_size = NULL;
			int frg_get = 0;
			int frg_max = 0;
			int size = 0;
			// Envoi de la demande du hash

			send_paquet(p_get_chunk, sockfd, (struct sockaddr *) &seeder, addrlen);	
			
			int chunk_received = 0;
			while(!chunk_received){  // Reception des morceaux de fichiers (1kb max/fragment)
				
				// Si on a tous, on verifie attend plus les fragements
				if(frg_get >= frg_max && fragments != NULL) chunk_received = 1;
				else{
			
					struct paquet p_recu = take_paquet(sockfd,  (struct sockaddr *) &listen, &addrlen);

					//if(DEBUG) debug_paquet_serial(p_recu);

					if(p_recu.msg.type == T_PP_REP_GET){ // On recoie un fragment
						struct c_fragment * data = (struct c_fragment *) p_recu.msg.data;

						if(fragments == NULL){ // Premier fragment
							fragments = (char **) malloc(sizeof(char *) * data->frg->max_index);
							frg_max = data->frg->max_index;
							fragments_size = (int *) malloc(sizeof(int) * frg_max);
						}

						int idx = data->frg->index - 1;
						fragments[idx] = (char *) malloc(sizeof(char) * (data->frg->size - 4));
						fragments_size[idx] = data->frg->size - 4;
						memcpy(fragments[idx], data->frg->data, sizeof(char) * (data->frg->size - 4));
						//printf("RECEPETION DE \n\n\nFIN%sDEBUT\n\n\n", fragments[idx]);

						size += data->frg->size - 4;
						++frg_get;

						p_recu.msg.type = T_PP_REP_GET_ACK;
						send_paquet(p_recu, sockfd, (struct sockaddr *) &seeder, addrlen);	


					}
					//printf("FRG max : %d, FRG Get : %d\n", frg_max, frg_get);
				}


			}

			// On verifie si l'empreinte est identique a celle de ce que l'on a recu
			
			//printf("Chunk Size is %d\n", size);
			// Creation d'une chaine contenant tout
			char chunk_content[size+1];
			char *ptr = chunk_content;
			for(i = 0; i < frg_max; ++i){
				memcpy(ptr, fragments[i], fragments_size[i]);
				ptr += fragments_size[i];
			}
			chunk_content[size] = '\0';
			
			//printf("CHUNK CONTENT \n\nDEBUT%sFIN\n\n", chunk_content);
			char fragments_hash[SHA256_HASH_LENGTH];
			string_sha256_l(chunk_content, fragments_hash, size);

			//printf("%s\n", fragments_hash);
		
			// Debug, creation d'un fichier contenant la valeur du chunk recu
			/*	
			char chunk_fname[size+60];
			sprintf(chunk_fname, "%s_%d_%s.chunk", argv[5], chunk_process, fragments_hash);
			FILE * chunk_write = fopen(chunk_fname, "w");

			for(i = 0; i < size; ++i){
				putc(chunk_content[i], chunk_write);
			}

			fclose(chunk_write);*/

			if(memcmp(fragments_hash, ch.hash, SHA256_HASH_LENGTH) == 0){
				acquis[chunk_process] = 1;			// Si empreinte identique, alors on continue
				chunks_content[chunk_process] = (char *) malloc(sizeof(char) * size);
				memcpy(chunks_content[chunk_process], chunk_content, size); // Enregistrement du contenu du chunk
				chunks_size[chunk_process] = size;
				printf("chunk %d acquis\n", chunk_process+1);
			}

			for(i = 0; i < nb_chunk ; ++i){ 		// Verifie si on a tous les chunks
				if(!acquis[i]) i = nb_chunk;		// Si 1 n'est pas acquis, on continue
				else if(i == nb_chunk - 1) fini = 1;	// Si le dernier est acquis, alors les precendants aussi et on arrete
			}

		}

		// Assemblage des chunks
		/*
		int file_size = 0;
		for(i = 0; i < nb_chunk; ++i){
			file_size += chunks_size[i];
		}
		
		char file_content[file_size + 1];
		char * write_ptr = file_content;
		for(i = 0; i < nb_chunk; ++i){
			memcpy(write_ptr, chunks_content[i], chunks_size[i]);
			write_ptr += chunks_size[i];
		}
		file_content[file_size] = '\0';
		printf("FILE RECEIVED \n\n\nDEBUG------%s------FIN\n\n\n", file_content);*/

		FILE * file_write;
		file_write = fopen(argv[5], "w");
		for(i = 0; i < nb_chunk; ++i){
			int j;
			for(j = 0; j < chunks_size[i]; ++j){
				putc(chunks_content[i][j], file_write);
			}
		}

		//putc('\0', file_write); Non nécessaire
		fclose(file_write);
		printf("Fichier %s correctement télechargé.\n", argv[5]);






	}
	// close the socket
	close(sockfd);

	exit(0);

}
