/*
 * hashlib.c
 *
 *  Created on: Nov 1, 2016
 */

#include "hashlib.h"
#include "utils.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

int fchunks(const char * file_url){
	FILE *file;
	file = fopen(file_url, "r");
	if (!file) perror("Can't open the file"); // Si fopen n'a pas marcher

	// Calculer la taille du fichier
	fseek(file, 0, SEEK_END); // defini la position vers la fin du fichier
	long size = ftell(file); // recupere l'index de la position
	fseek(file, 0, SEEK_SET); // remet la position au debut

	// Calcul du nombre de chunk
	if (CHUNK_SIZE > size){
		return 1;
	}else{
		return size/CHUNK_SIZE + 1;
	}

}

void file_sha256(const char * file_url, char output[SHA256_HASH_LENGTH]){

	FILE *file;
	file = fopen(file_url, "r");
	if (!file) perror("Can't open the file"); // Si fopen n'a pas marcher

	unsigned char hash[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha_hash;

	// Initialisation du hash
	SHA256_Init(&sha_hash);

	unsigned char buffer[CHUNK_SIZE];

	int nbItemsRead = 0;

	// Tant qu'on peut lire dans le fichier
	// Des tableaux de taille CHUNK SIZE contenant des bytes de taille 1
	while((nbItemsRead = fread(buffer, 1, CHUNK_SIZE, file))){
		// Ajout du bloc a l'empreinte
		SHA256_Update(&sha_hash, buffer, nbItemsRead);
	}
	// Ajout de l'empreinte dans la variable hash
	SHA256_Final(hash, &sha_hash);

	// Conversion de la variable hash vers une chaine de caractere
	sha256_to_string(hash, output);

}

void file_chunk_sha256(const char * file_url, char output[SHA256_HASH_LENGTH], const int chunk_index){
	FILE *file;
	file = fopen(file_url, "r");
	if (!file) perror("Can't open the file"); // Si fopen n'a pas marcher

	assert(chunk_index <= fchunks(file_url));

	unsigned char hash[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha_hash;

	// Initialisation du hash
	SHA256_Init(&sha_hash);

	unsigned char buffer[CHUNK_SIZE];

	int nbItemsRead = 0;
	int current_chunk = 1;
	// Tant qu'on peut lire dans le fichier
	// Des tableaux de taille CHUNK SIZE contenant des bytes de taille 1
	while((nbItemsRead = fread(buffer, 1, CHUNK_SIZE, file))){
		if(current_chunk == chunk_index){
			// Ajout du bloc a l'empreinte
			SHA256_Update(&sha_hash, buffer, nbItemsRead);
			break;
		}
		++current_chunk;
	}
	// Ajout de l'empreinte dans la variable hash
	SHA256_Final(hash, &sha_hash);

	// Conversion de la variable hash vers une chaine de caractere
	sha256_to_string(hash, output);
}

void string_sha256(const char * input, char output[SHA256_HASH_LENGTH]){

	unsigned char hash[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha_hash;

	// Initialisation du hash
	SHA256_Init(&sha_hash);
	// Creation de l'empreinte de l'entrée
	SHA256_Update(&sha_hash, input, strlen(input));
	// Ajout de l'empreinte dans la variable hash
	SHA256_Final(hash, &sha_hash);

	// Conversion de la variable hash vers une chaine de caractere
	sha256_to_string(hash, output);
}

void string_sha256_l(const char * input, char output[SHA256_HASH_LENGTH], const int size){

	unsigned char hash[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha_hash;

	// Initialisation du hash
	SHA256_Init(&sha_hash);
	// Creation de l'empreinte de l'entrée
	SHA256_Update(&sha_hash, input, size);
	// Ajout de l'empreinte dans la variable hash
	SHA256_Final(hash, &sha_hash);

	// Conversion de la variable hash vers une chaine de caractere
	sha256_to_string(hash, output);
}


void sha256_to_string(unsigned char hash[SHA256_DIGEST_LENGTH], char output[SHA256_HASH_LENGTH]){
	int i = 0;
	for(i = 0; i < SHA256_DIGEST_LENGTH; i++)
	{
		sprintf(output + (i * 2), "%02x", hash[i]); // Transfer l'octet dans output sous forme de char
	}
	output[64] = 0;
}
