/*
 * q1-2_tracker.c
 *
 *  Created on: Nov 4, 2016
 */

#include "utils.h"
#include "peerslib.h"
#include "trackerlib.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <time.h>
#include <signal.h>
int main( int argc, const char* argv[] )
{

	struct tracker_clients tracker_clients_buffer = {NULL, NULL, 0};


    // Check for args
    if( argc != 3 ){
        fail("USAGE: $q1-2_tracker address port", EXIT_FAILURE);
    }


    // Creation du socket

    int sockfd = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
    if(sockfd == -1)
	{
		failp("A problem has occurred while creating the socket");
	}

    // Creation des sockets ecout/envoi

	struct sockaddr_in6 me;
	if(create_sock6(&me, argv[2], AF_INET6, argv[1]) < 0){
		failp("Can't create the socket addr for the tracker");
	}

	struct sockaddr_in6 sock_ecoute ;
	memset (&sock_ecoute, 0, sizeof (struct sockaddr_in6));

	socklen_t addrlen = sizeof(struct sockaddr_in6); // Taille adresse

	// Output terminal

	char * listen_addr = get_addr_string(&me.sin6_addr.s6_addr);
	if(listen_addr == NULL){
		failp("Cannot get the address value");
	}
	printf("listening on %s port %s\n", listen_addr, argv[2]);



	// bind addr structure with socket
	if(bind(sockfd, (struct sockaddr *) &me, addrlen) == -1)
	{
	  perror("bind");
	  close(sockfd);
	  exit(EXIT_FAILURE);
	}

	// Fonction de fermeture signal close
	void close_socket(int signo){
		if(signo == SIGINT){
			close(sockfd);
			exit(0);
		}
	}

	if(signal(SIGINT, close_socket) == SIG_ERR)
		fail("Can't bind SIGINT", EXIT_FAILURE);


	// Ecoute
	while(1){

		// reception d'un paquet'
		struct paquet p_recu = take_paquet(sockfd,  (struct sockaddr *) &sock_ecoute, &addrlen);
		recycle_tracker_clients(&tracker_clients_buffer); // On supprime les vieux clients sans keep alive

		if(p_recu.msg.type == T_CT_PUT){ // SI le message recu est un PUT

			if(DEBUG) printf("Reception d'un paquet de type put\n");

			struct c_file_hash_client * content = (struct c_file_hash_client *) p_recu.msg.data;
			// Ajout du client au buffer du tracker
			memcpy(content->c.address, sock_ecoute.sin6_addr.s6_addr, 16); // Recuperation de l'addresse du socket dans le client
			ajouter_client(&tracker_clients_buffer, content->c, content->fh);
			p_recu.msg.type = T_CT_ACK_PUT; // On renvoi un ACK

		}else if(p_recu.msg.type == T_CT_GET){ // Si le message recu est un GET

			if(DEBUG) printf("Reception d'un paquet de type get client\n");

			struct c_file_hash_client * content = (struct c_file_hash_client *) p_recu.msg.data;
			p_recu.msg.type = T_CT_ACK_GET; // On renvoi un ACK
			
			struct c_file_hash_liste_client * clients_send = (struct c_file_hash_liste_client *) malloc(sizeof(struct c_file_hash_liste_client));

			clients_send->fh = &(content->fh);

			
			struct tracker_unique_client *ptr_read = tracker_clients_buffer.first;
			struct client_chained * previous = NULL;
			struct client_chained * add;


			// Pour chaque clients dans le buffer du tracker
			// Creation d'un pointer vers ce client
			// Ce qui fait une liste chainée de client qu'on met dans le paquet

			int i;
			for(i = 0; i < tracker_clients_buffer.count; ++i){
				if(strcmp(clients_send->fh->hash, ptr_read->hash.hash) == 0){ // On retourne seulements les clients qui ont le hash demandé
				
					add = (struct client_chained *) malloc(sizeof(struct client_chained));
					add->value = &(ptr_read->client);
					add->next = NULL;
					if(previous != NULL) previous->next = add;
					else clients_send->first = add;
					previous = add;

				}

				ptr_read = ptr_read->next;
	
			}

			//free(content);
			p_recu.msg.data = (void *) clients_send;

		}else if(p_recu.msg.type == T_CT_KEEP_ALIVE){ // Reception d'un keep alive
			// TO DO : reset timeout client
			struct c_file_hash_client * content = (struct c_file_hash_client *) p_recu.msg.data;
			keep_alive_tracker_client(&tracker_clients_buffer, content->c, content->fh);
			p_recu.msg.type = T_CT_ACK_KEEP_ALIVE;

		}

		if(DEBUG) debug_paquet_serial(p_recu);

		send_paquet(p_recu, sockfd, (struct sockaddr *) &sock_ecoute, addrlen); // Envoie du paquet
		free(p_recu.msg.data); 							// Liberation de la memoire associé au paquet
		afficher_tracker_clients(&tracker_clients_buffer); 			// Affichage du contenu du tracker

		if(DEBUG) printf("\n\n===========\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n==================\n\n");


	}


	// close the socket
	close(sockfd);

	exit(0);

}
