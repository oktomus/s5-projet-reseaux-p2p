#include "peerslib.h"
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>



unsigned char * serialize_unshort(unsigned char *buffer, unsigned short s){
	unsigned short s_ordered = htons(s);
	unsigned char *c = (unsigned char *) &s_ordered;
	buffer[0] = c[0];
	buffer[1] = c[1];
	return buffer + 2;
}

unsigned char * serialize_string(unsigned char *buffer, unsigned char *c, unsigned short taille)
{
	int j;
	for (j = 0; j < taille; ++j) {
		buffer[j] = c[j];
	}
	return buffer + taille;
}

unsigned char * serialize_unchar(unsigned char *buffer, unsigned char c)
{
	buffer[0] = c;
	return buffer + 1;
}

unsigned char * serialize_file_hash(unsigned char *buffer, struct file_hash * fh)
{
	buffer = serialize_unchar(buffer, fh->type);
	buffer = serialize_unshort(buffer, fh->size);
	buffer = serialize_string(buffer, fh->hash, fh->size);
	return buffer;
}

unsigned char * serialize_chunk_hash(unsigned char *buffer, struct chunk_hash * value){
	buffer = serialize_unchar(buffer, value->type);
	buffer = serialize_unshort(buffer, value->size);
	buffer = serialize_string(buffer, value->hash, value->size - 2);
	buffer = serialize_unshort(buffer, value->index);
}

unsigned char * serialize_client(unsigned char *buffer, struct client * value)
{
	buffer = serialize_unchar(buffer, value->type);
	buffer = serialize_unshort(buffer, value->length);
	buffer = serialize_unshort(buffer, value->port);
	buffer = serialize_string(buffer, value->address, value->length - 2);
	return buffer;
}

unsigned char * serialize_fragment(unsigned char *buffer, struct fragment * value)
{
	buffer = serialize_unchar(buffer, value->type);
	buffer = serialize_unshort(buffer, value->size);
	buffer = serialize_unshort(buffer, value->index);
	buffer = serialize_unshort(buffer, value->max_index);
	buffer = serialize_string(buffer, value->data, value->size - 4);
	return buffer;
}

unsigned char * serialize_message(unsigned char *buffer, struct message *msg)
{
	buffer = serialize_unchar(buffer, msg->type);

	//unsigned char *temp_pointer = buffer;
	unsigned char buffer_2[BUFFER_SIZE], *pnt_buf_2;
	pnt_buf_2 = NULL;
	if(msg->type == T_CT_PUT || msg->type == T_CT_ACK_PUT || msg->type == T_CT_GET || msg->type == T_CT_KEEP_ALIVE || msg->type == T_CT_ACK_KEEP_ALIVE){
		struct c_file_hash_client * data = (struct c_file_hash_client *) msg->data;
		pnt_buf_2 = serialize_file_hash(buffer_2, &(data->fh));
		pnt_buf_2 = serialize_client(pnt_buf_2, &(data->c));
	}else if(msg->type == T_CT_ACK_GET){
		struct c_file_hash_liste_client * data = (struct c_file_hash_liste_client *) msg->data;
		pnt_buf_2 = serialize_file_hash(buffer_2, data->fh);
		struct client_chained * ptr_client = data->first;
		while(ptr_client != NULL){
			pnt_buf_2 = serialize_client(pnt_buf_2, ptr_client->value);
			ptr_client = ptr_client->next;
		}	
	}else if(msg->type == T_PP_LIST){
		struct file_hash * data = (struct file_hash *) msg->data;
		pnt_buf_2 = serialize_file_hash(buffer_2, data);
	}else if(msg->type == T_PP_REP_LIST){
		struct c_file_hash_liste_chunk * data = (struct c_file_hash_liste_chunk *) msg->data;
		pnt_buf_2 = serialize_file_hash(buffer_2, data->fh);
		struct chunk_hash_chained * ptr_chunk = data->first;
		while(ptr_chunk != NULL){
			pnt_buf_2 = serialize_chunk_hash(pnt_buf_2, ptr_chunk->value);
			ptr_chunk = ptr_chunk->next;
		}

	}else if(msg->type == T_PP_GET){
		struct c_file_chunk_hash * data = (struct c_file_chunk_hash *) msg->data;
		pnt_buf_2 = serialize_file_hash(buffer_2, data->fh);
		pnt_buf_2 = serialize_chunk_hash(pnt_buf_2, data->ch);
	}else if(msg->type == T_PP_REP_GET || msg->type == T_PP_REP_GET_ACK){
		struct c_fragment * data = (struct c_fragment *) msg->data;
		pnt_buf_2 = serialize_file_hash(buffer_2, data->fh);
		pnt_buf_2 = serialize_chunk_hash(pnt_buf_2, data->ch);
		pnt_buf_2 = serialize_fragment(pnt_buf_2, data->frg);

	}


	if(pnt_buf_2 != NULL){
		msg->length = pnt_buf_2 - buffer_2;
		if(msg->length){
			buffer = serialize_unshort(buffer, msg->length);
			buffer = serialize_string(buffer, buffer_2, msg->length);
		}
	}
	return buffer;
}

unsigned char * serialize_paquet(unsigned char *buffer, struct paquet *value)
{
	buffer = serialize_message(buffer, &(value->msg));


	return buffer;
}

unsigned short unserialize_unshort(unsigned char **buffer){
	unsigned short res;
	res = ntohs((*buffer)[1] << 8 | (*buffer)[0]);
	*buffer = *buffer + 2;
	return res;
}


unsigned char unserialize_unchar(unsigned char **buffer)
{
	unsigned char res = (*buffer)[0];
	*buffer = *buffer + 1;
	return res;
}

unsigned char * unserialize_string(unsigned char **buffer, unsigned short taille){
	unsigned char * res = (unsigned char *) malloc((taille) * sizeof(unsigned char));
	int j;
	for (j = 0; j < taille; ++j) {
		res[j] = (*buffer)[j];
	}
	*buffer = *buffer + taille;
	return res;
}

struct file_hash unserialize_file_hash(unsigned char **buffer)
{
	struct file_hash res;

	res.type = unserialize_unchar(buffer);
	res.size = unserialize_unshort(buffer);
	res.hash = unserialize_string(buffer, res.size);
	return res;
}

struct chunk_hash unserialize_chunk_hash(unsigned char **buffer){
	struct chunk_hash res;
	res.type = unserialize_unchar(buffer);
	res.size = unserialize_unshort(buffer);
	res.hash = unserialize_string(buffer, res.size - 2);
	res.index = unserialize_unshort(buffer);

	return res;
}

struct client unserialize_client(unsigned char **buffer)
{
	struct client res;
	res.type = unserialize_unchar(buffer);
	res.length = unserialize_unshort(buffer);
	res.port = unserialize_unshort(buffer);
	res.address = unserialize_string(buffer, res.length - 2);
	return res;
}

struct fragment unserialize_fragment(unsigned char **buffer){
	struct fragment res;
	res.type = unserialize_unchar(buffer);
	res.size = unserialize_unshort(buffer);
	res.index = unserialize_unshort(buffer);
	res.max_index = unserialize_unshort(buffer);
	res.data = unserialize_string(buffer, res.size - 4);
	return res;
}


struct message unserialize_message(unsigned char **buffer){
	struct message res;
	res.type = unserialize_unchar(buffer);
	res.length = unserialize_unshort(buffer);
	res.data = NULL;

	if(res.type == T_CT_PUT || res.type == T_CT_ACK_PUT || res.type == T_CT_GET || res.type == T_CT_KEEP_ALIVE || res.type == T_CT_ACK_KEEP_ALIVE){ // PUT [aCk] ou get
		struct c_file_hash_client * data = (struct c_file_hash_client *) malloc(sizeof(unsigned char) * res.length);
		data->fh = unserialize_file_hash(buffer);
		data->c = unserialize_client(buffer);
		res.data = (void *) data;

	}else if(res.type == T_CT_ACK_GET){

		struct c_file_hash_liste_client * data = (struct c_file_hash_liste_client *) malloc(sizeof(struct c_file_hash_liste_client));
		struct file_hash fh = unserialize_file_hash(buffer);
		data->fh = (struct file_hash *) malloc(sizeof(struct file_hash) + fh.size * sizeof(char));
		memcpy(data->fh, &fh, sizeof(struct file_hash) + fh.size * sizeof(char));
		data->first = NULL;
		struct client_chained * last = NULL;
		while((*buffer)[0] == T_C_CLIENT){
			struct client c = unserialize_client(buffer);
			struct client * c_ptr = (struct client *) malloc(sizeof(struct client) + (c.length - 2) * sizeof(unsigned char));
			memcpy(c_ptr, &c, sizeof(struct client) + (c.length - 2) * sizeof(unsigned char));

			if(last == NULL){
				data->first = (struct client_chained *) malloc(sizeof(struct client_chained));
				data->first->value = c_ptr;
				data->first->next = NULL;
				last = data->first;
			}else{
				last->next = (struct client_chained *) malloc(sizeof(struct client_chained));
				last->next->value = c_ptr;
				last->next->next = NULL;
				last = last->next;

			}
		}

		res.data = (void *) data;
	}else if(res.type == T_PP_LIST){

		struct file_hash * data = (struct file_hash *) malloc(sizeof(unsigned char) * res.length);
		*data = unserialize_file_hash(buffer);
		res.data = (void *) data;
	}else if(res.type == T_PP_REP_LIST){
		struct c_file_hash_liste_chunk * data = (struct c_file_hash_liste_chunk *) malloc(sizeof(struct c_file_hash_liste_chunk));
		struct file_hash fh = unserialize_file_hash(buffer);
		data->fh = (struct file_hash *) malloc(sizeof(struct file_hash) + fh.size * sizeof(char));
		memcpy(data->fh, &fh, sizeof(struct file_hash) + fh.size * sizeof(char));
		data->first = NULL;
		struct chunk_hash_chained * last = NULL;
		data->size = 0;
		while((*buffer)[0] == T_H_CHUNK){
			struct chunk_hash c = unserialize_chunk_hash(buffer);
			struct chunk_hash * c_ptr = (struct chunk_hash *) malloc(sizeof(struct chunk_hash) + (c.size - 2) * sizeof(unsigned char));
			memcpy(c_ptr, &c, sizeof(struct chunk_hash) + (c.size - 2) * sizeof(unsigned char));
			++(data->size);
			if(last == NULL){
				data->first = (struct chunk_hash_chained *) malloc(sizeof(struct chunk_hash_chained));
				data->first->value = c_ptr;
				data->first->next = NULL;
				last = data->first;
			}else{
				last->next = (struct chunk_hash_chained *) malloc(sizeof(struct chunk_hash_chained));
				last->next->value = c_ptr;
				last->next->next = NULL;
				last = last->next;

			}
		}

		res.data = (void *) data;

	}else if(res.type == T_PP_GET){
		struct c_file_chunk_hash * data = (struct c_file_chunk_hash *) malloc(sizeof(struct c_file_chunk_hash));
		struct file_hash fh = unserialize_file_hash(buffer);
		data->fh = (struct file_hash *) malloc(sizeof(struct file_hash) + fh.size * sizeof(char));
		memcpy(data->fh, &fh, sizeof(struct file_hash) + fh.size * sizeof(char));

		struct chunk_hash ch = unserialize_chunk_hash(buffer);
		data->ch = (struct chunk_hash *) malloc(sizeof(struct chunk_hash) + (ch.size - 2) * sizeof(char));
		memcpy(data->ch, &ch, sizeof(struct chunk_hash) + (ch.size - 2) * sizeof(char));

		res.data = (void *) data;
	}else if(res.type == T_PP_REP_GET || res.type == T_PP_REP_GET_ACK){
		struct c_fragment * data = (struct c_fragment *) malloc(sizeof(struct c_fragment));

		struct file_hash fh = unserialize_file_hash(buffer);
		data->fh = (struct file_hash *) malloc(sizeof(struct file_hash) + fh.size * sizeof(char));
		memcpy(data->fh, &fh, sizeof(struct file_hash) + fh.size * sizeof(char));

		struct chunk_hash ch = unserialize_chunk_hash(buffer);
		data->ch = (struct chunk_hash *) malloc(sizeof(struct chunk_hash) + (ch.size - 2) * sizeof(char));
		memcpy(data->ch, &ch, sizeof(struct chunk_hash) + (ch.size - 2) * sizeof(char));

		struct fragment frg = unserialize_fragment(buffer);
		data->frg = (struct fragment *) malloc(sizeof(struct fragment) + (frg.size -4) * sizeof(char));
		memcpy(data->frg, &frg, sizeof(struct fragment) + (frg.size -4) * sizeof(char));

		res.data = (void *) data;

	}
	return res;
}

struct paquet unserialize_paquet(const char *buffer){
	struct paquet res;
	unsigned char * ptr = (unsigned char *) buffer;
	res.msg = unserialize_message(&ptr);
	return res;
}


int create_sock6(struct sockaddr_in6 *sock, const char* port, sa_family_t sock_family, const char* address){
	memset (sock, 0, sizeof (struct sockaddr_in6));


	sock->sin6_family		  	= AF_INET6;
	sock->sin6_port		  		= htons(atoi(port));

	if (address != NULL){
		if(inet_pton(AF_INET6, address, &(sock->sin6_addr)) != 1){ // Converti la chaine en adresse ip v6
			return -1;
		}
	}

	return 0;
}

void old_get_addr_string6(struct sockaddr_in6 *sock, char * output){
	inet_ntop(AF_INET6, &sock->sin6_addr, output, INET6_ADDRSTRLEN);
}

char * get_addr_string(const void *src){
	char * output = (char *) malloc(sizeof(char) * INET6_ADDRSTRLEN);
	inet_ntop(AF_INET6, src, output, INET6_ADDRSTRLEN);
	return output;
}

struct paquet take_paquet(int sockfd, struct sockaddr * src, socklen_t * addrlen){
	char buf[BUFFER_SIZE];
	memset(buf,'\0',BUFFER_SIZE);
	int msg_size = recvfrom(sockfd, buf, BUFFER_SIZE, 0, src, addrlen);

	if(msg_size == -1){
		perror("recvfrom");
		close(sockfd);
		exit(EXIT_FAILURE);
	}

	buf[BUFFER_SIZE - 1] = '\0';
	//debug_paquet_unserial(buf, msg_size);
	struct paquet p = unserialize_paquet(buf);
	return p;
}

void send_paquet(struct paquet p, int sockfd, struct sockaddr * dest, socklen_t addrlen){
	// Buffer de sortie
	unsigned char buf_envoi[BUFFER_SIZE];
	memset(buf_envoi,'\0',BUFFER_SIZE);

	unsigned char *ptr_end;
	ptr_end = serialize_paquet(buf_envoi, &p);
	int size = ptr_end - buf_envoi;

	// Envoie d'un ack au client
	if(sendto(sockfd, buf_envoi, size, 0, dest, addrlen) == -1)
	{
		perror("sendto");
		close(sockfd);
		exit(EXIT_FAILURE);
	}
}


void debug_paquet_serial(struct paquet p){
	printf("---- Debug un paquet pour serialisation -----\n\n");

	printf("Type : %d\n", p.msg.type);
	printf("Length : %d (will change after serialization) \n\n", p.msg.length);
	int type = p.msg.type;
	if(
			type == T_CT_ACK_PUT 
			|| type == T_CT_PUT
			|| type == T_CT_ACK_KEEP_ALIVE
			|| type == T_CT_KEEP_ALIVE
			|| type == T_CT_GET
	  ){
		printf("-- File hash -- \n");
		struct c_file_hash_client *ack_put_get = (struct c_file_hash_client*) p.msg.data ;
		printf("file hash type : %d\n", ack_put_get->fh.type);
		printf("file hash size : %d\n", ack_put_get->fh.size);
		printf("file hash value : '%s'\n", ack_put_get->fh.hash);
		printf("-- Client --\n");
		printf("client type : %d\n", ack_put_get->c.type);
		printf("client length : %d\n", ack_put_get->c.length);
		printf("client port : %d\n", ack_put_get->c.port);
		printf("client address : %s\n", ack_put_get->c.address);
	}else if(
			type == T_CT_ACK_GET
		){
		printf("-- File hash -- \n");
		struct c_file_hash_liste_client *ack_get = (struct c_file_hash_liste_client*) p.msg.data ;
		printf("file hash type : %d\n", ack_get->fh->type);
		printf("file hash size : %d\n", ack_get->fh->size);
		printf("file hash value : '%s'\n", ack_get->fh->hash);
		struct client_chained * ptr = ack_get->first;
		while(ptr != NULL){
			struct client * c = ptr->value;
			printf("-- Client --\n");
			printf("client type : %d\n", c->type);
			printf("client length : %d\n", c->length);
			printf("client port : %d\n", c->port);
			printf("client address : %s\n", get_addr_string(c->address));
			ptr = ptr->next;
		}
	}else if(
			type == T_PP_LIST
		){
		printf("-- File hash -- \n");
		struct file_hash * file_hash = (struct file_hash *) p.msg.data ;
		printf("file hash type : %d\n", file_hash->type);
		printf("file hash size : %d\n", file_hash->size);
		printf("file hash value : '%s'\n", file_hash->hash);
	}else if(			
			type == T_PP_REP_LIST
		){
		printf("-- File hash -- \n");
		struct c_file_hash_liste_chunk * liste_chunk = (struct c_file_hash_liste_chunk *) p.msg.data;
		printf("file hash type : %d\n", liste_chunk->fh->type);
		printf("file hash size : %d\n", liste_chunk->fh->size);
		printf("file hash value : '%s'\n", liste_chunk->fh->hash);
		printf("-- Chunks -- \n");
		printf("%d chunk(s)\n", liste_chunk->size);
		struct chunk_hash_chained * ptr = liste_chunk->first;
		while(ptr != NULL){
			printf("CHUNK %d : %s\n", ptr->value->index, ptr->value->hash);
			ptr = ptr->next;
		}
	}else if(
			type == T_PP_GET
		){
		printf("-- File hash -- \n");
		struct c_file_chunk_hash * data = (struct c_file_chunk_hash *) p.msg.data;
		printf("file hash type : %d\n", data->fh->type);
		printf("file hash size : %d\n", data->fh->size);
		printf("file hash value : '%s'\n", data->fh->hash);
		printf("-- Chunk -- \n");
		printf("chunk hash type : %d\n", data->ch->type);
		printf("chunk hash size : %d\n", data->ch->size);
		printf("chunk hash value : %s\n", data->ch->hash);
		printf("chunk hash index : %d\n", data->ch->index);
	}else if(
			type == T_PP_REP_GET
			|| type == T_PP_REP_GET_ACK
		){
		printf("-- File hash -- \n");
		struct c_fragment * data = (struct c_fragment *) p.msg.data;
		printf("file hash type : %d\n", data->fh->type);
		printf("file hash size : %d\n", data->fh->size);
		printf("file hash value : '%s'\n", data->fh->hash);
		printf("-- Chunk -- \n");
		printf("chunk hash type : %d\n", data->ch->type);
		printf("chunk hash size : %d\n", data->ch->size);
		printf("chunk hash value : %s\n", data->ch->hash);
		printf("chunk hash index : %d\n", data->ch->index);
		printf("-- Fragment --\n");
		printf("frg type : %d\n", data->frg->type);
		printf("frg size : %d\n", data->frg->size);
		printf("frg index : %d\n", data->frg->index);
		printf("frg max_index : %d\n", data->frg->max_index);


	}

	unsigned char serial[BUFFER_SIZE], *serial_end;
	serial_end = serialize_paquet(serial, &p);
	int size = serial_end - serial;

	printf("\n\nAffichage du paquet serialisé taille(%d) en char puis en hexa:\n", size);
	int j;
	for (j = 0; j < size; ++j) {
		printf("%c", serial[j]);
	}
	printf("\n");
	for (j = 0; j < size; ++j) {
		printf("%02x ", serial[j]);
	}
	printf("\n");


	printf("---- Fin de debug ---\n\n");
}


void debug_paquet_unserial(char * buff, ssize_t taille){
	printf("---- Debug un paquet pour désérialisation -----\n\n");

	printf("\n\nAffichage du buffer serialisé taille(%zu) en char puis en hexa:\n", taille);
	int j;
	for (j = 0; j < taille; ++j) {
		printf("%c", buff[j]);
	}
	printf("\n");
	for (j = 0; j < taille; ++j) {
		printf("%02x ", buff[j]);
	}
	printf("\n");
	struct paquet res = unserialize_paquet(buff);

	debug_paquet_serial(res);


	printf("---- Fin de debug ---\n\n");
}

