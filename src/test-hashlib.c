/*
 * test-hashlib.c
 *
 *  Created on: Nov 1, 2016
 */

#include "hashlib.h"
#include "assert.h"
#include <string.h>

int main( int argc, const char* argv[] )
{

	char strhash[SHA256_HASH_LENGTH];

	// TEST string_sha256

	string_sha256("basic", strhash);
	assert(strcmp(strhash, "fbb7b5bf4ca348eeeebf8cc3660712859a5a1fbf18e83217a57ce203fb51d5c2") == 0);

	string_sha256("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla est vel purus efficitur, vel efficitur erat vehicula. Suspendisse potenti. Pellentesque a mattis turpis. Fusce facilisis ante quis urna sollicitudin auctor. Aenean bibendum, felis eu tempor dapibus, lorem tortor pharetra lacus, sit amet suscipit arcu mi eget metus. Aenean tristique nisi ut nisi sagittis luctus. Sed venenatis massa vestibulum odio convallis, in semper turpis euismod. Quisque ultricies viverra leo, ac congue tortor fermentum vel.", strhash);
	assert(strcmp(strhash, "9d8f5d0932a0b906e2e5b9e2c31c372b0c696988bf088cfb5b239fca4b4d9144") == 0);


	// TEST file_sha256

	file_sha256("./LICENSE", strhash);
	assert(strcmp(strhash, "addbe194edc9bc2c9cedb27de9505e575eb57a5f359e1940ee99965697f0fc9a") == 0);

	return 0;


}

