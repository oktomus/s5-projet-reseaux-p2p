#ifndef SRC_TRACKERLIB_H_
#define SRC_TRACKERLIB_H_

#include "peerslib.h"

#define TRACKER_BUFFER_TIMEOUT 		15 // Le temps en secondes apres lequel le tracker supprime un client de son buffer si aucun Keep alive recu


struct tracker_unique_client{
	struct client client;
	struct file_hash hash;
	int id; // Used to remove a client with a timeout
	struct tracker_unique_client *next;
};

struct tracker_clients{
	struct tracker_unique_client *first;
	struct tracker_unique_client *last;
	int count;
};

/*
 * Supprime les clients dont on a pas recu de keep alive depuis TRACKER_BUFFER_TIMEOUT secondes
 */
void recycle_tracker_clients(struct tracker_clients * buf);

void keep_alive_tracker_client(struct tracker_clients * buf, struct client cl, struct file_hash hs);

void ajouter_client(struct tracker_clients * buf, struct client cl, struct file_hash hs);

void afficher_tracker_clients(struct tracker_clients * buf);

int enlever_client(struct tracker_clients * buf, int id);

#endif /* SRC_TRACKERLIB_H_ */
