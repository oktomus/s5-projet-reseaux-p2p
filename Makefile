CC=gcc # -g
CFLAGS=-Isrc
LIBS=-lcrypto
DEPS= src/hashlib.h src/utils.h src/trackerlib.h src/peerslib.h
OBJS= build/hashlib.o build/utils.o build/peerslib.o build/trackerlib.o
EX= build/hash-file build/test-hashlib build/q1-2_tracker build/q1-2_client build/q1-4_seeder build/q1-4_leecher build/frag_file

all: $(EX)

build/%.o: src/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS) $(LIBS)

build/hash-file: build/hash-file.o $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)
	
build/test-hashlib: build/test-hashlib.o $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)
	
build/q1-2_tracker: build/q1-2_tracker.o $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)
	
build/q1-2_client: build/q1-2_client.o $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)
	
build/q1-4_seeder: build/q1-4_seeder.o $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)
	
build/q1-4_leecher: build/q1-4_leecher.o $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

build/frag_file: build/frag_file.o
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f build/*.o build/*.a $(EX)
